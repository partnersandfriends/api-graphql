FROM node:13.7.0-alpine3.10

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

COPY . .
EXPOSE 4005
ENV API_GRAPHQL_PORT=4005
ENV API_HOST=http://localhost:4011
ENV MIDDLEWARE_JAVA_HOST=http://localhost:8081
CMD ["node", "index.js"]