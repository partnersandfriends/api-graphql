const { ApolloServer } = require('apollo-server');
const schema = require('./graphql');
const formatError = require('./graphql/formatError');
const propagateHeaders = require('./lib/propagateHeaders');

const server = new ApolloServer({
  schema,
  formatError,
  context: ({ req }) => {
    propagateHeaders(req.headers);
  },
  playground: true,
});
server.listen({ port: process.env.API_GRAPHQL_PORT }).then(({ url }) => {
  process.stdout.write(`Running a GraphQL API server at ${url}graphql`);
});
