const { ApolloError } = require('apollo-server');
const axios = require('axios');

const findExpense = ({ id }) => {
  return axios
    .get(`${process.env.API_HOST}/expense/${id}`)
    .then(({ data }) => {
      return data;
    })
    .catch(({ message, response }) => {
      return new ApolloError(message, response.status);
    });
};
const findDetailById = (idUser, idDetail) =>
  axios
    .get(`${process.env.API_HOST}/expense/${idUser}/detail/${idDetail}`)
    .then(({ data }) => {
      return data;
    });

const findResources = () =>
  axios
    .get(`${process.env.MIDDLEWARE_JAVA_HOST}/chamadaApi`)
    .then(({ data }) => {
      return data;
    });

const simulate = simulation =>
  axios
    .post(`${process.env.API_SIMULATION_HOST}/simulation`, simulation)
    .then(({ data }) => {
      return data;
    });

const service = {
  details: {
    findById: findDetailById,
  },
  expenses: {
    find: findExpense,
  },
  account: {
    findResources,
  },
  simulation: {
    simulate,
  },
};

module.exports = service;
