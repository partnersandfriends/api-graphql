const axios = require('axios');

const propagateHeaders = headers => {
  Object.keys(headers).forEach(key => {
    if (key.startsWith('x-')) {
      axios.defaults.headers.common[key] = headers[key];
    } else if (key.toLowerCase() === 'authorization') {
      axios.defaults.headers.common.Authorization = headers[key];
    }
  });
};

module.exports = propagateHeaders;
