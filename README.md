# API GraphQL

Você está na versão 2 do Projeto de exemplo API GraphQL, não se esqueça de olhar a lista de TAGs para acompanhar a evolução do projeto

## Cenário 1: Usando o docker como recurso externo de um Banco de Dados e uma API REST

Para facilitar a configuração dos recursos externos, vamos utilizar o arquivo `docker-compose.yml`.

```bash
docker-compose up -d
```

# Criando variavél de ambiente para acessar API REST e o Banco de Dados

```bash
export API_HOST=http://localhost:4011
```

# Criando variavél de ambiente para liberar a porta onde a API GraphQL será disponibilizada

```bash
export API_GRAPHQL_PORT=4005
```

# Criando variavél de ambiente para acessar o middleware JAVA

```bash
export MIDDLEWARE_JAVA_HOST=http://localhost:8081
```

# Criando variavél de ambiente para acessar API Node de simulação

```bash
export API_SIMULATION_HOST=http://localhost:8084
```

# Executando a aplicação

```bash
npm run start
```

### OK tudo funcionando mas não temos nenhum registro para consultar, vamos resolver isso executando o seguinte comando contra a nossa API REST externa.

Abra um terminal e execute o comando abaixo

```bash
curl -X POST \
  http://localhost:4011/expense \
  -H 'Content-Type: application/json' \
  -d '{
    "idUser": "5000",
    "companyName": "Arcos Dourados",
    "value": 50.01,
    "details": {
        "cardNumber": "4716650221230609",
        "cnpj": "55.474.589/0001-51",
        "timeStamp": 1578243141,
        "mapLocation": "<seu-cdn>/av-paulista.png"
    }
}'
```

Acesse API GraphQL `http://localhost:4005/graph` e consulte o registro inserido usando a query abaixo

`{ expenses(idUser: "5000") { companyName details { cardNumber timeStamp mapLocation } } }`

Se preferir, você pode fazer a consulta diretamente pelo terminal

```bash
curl 'http://localhost:4005/graph'  -H 'Content-Type: application/json' -H 'Accept: application/json' -d '{"query":"{ expenses(idUser: \"5000\") { companyName details { cardNumber timeStamp mapLocation } } }"}'
```

# Finalizando o cenário 1

Pare a aplicação e remova os containers de recursos externos

```bash
docker-compose down
```

## Cenário 2: Usando o docker na aplicação

Gerando a imagem do projeto

```bash
docker build -t {user}/api-graphql:{version} .
```

docker build -t leviditomazzo/api-graphql:v1 .

### Executando a aplicação com docker-compose

```bash
docker-compose up -d
```

Removendo os containers criados

```bash
docker-compose down
```

## Docker Hub

Efetivando o login no Docker Hub

```bash
docker login
```

Enviando a imagem para o Docker Hub

```bash
docker push leviditomazzo/api-graphql:v1
```

Baixando a imagem do Docker Hub

Docker Pull Command

```bash
docker pull {user}/api-financial:{version}
```
