const service = require('../services');

const resolvers = {
  Expense: {
    details(parent) {
      // process.stdout.write('Consultando detalhes da despesa');
      return service.details.findById(parent.idUser, parent.details);
    },
  },
  Query: {
    expenses: (_, args) => {
      // process.stdout.write('Consultando a lista de despesa');
      return service.expenses.find({ id: args.idUser });
    },
    accountResources: () => service.account.findResources(),
  },
  Mutation: {
    simulate: (_, { simulation }) =>
      new Promise(resolve => {
        resolve(
          process.stdout.write(
            `\n Version 2 Simulation request: ${JSON.stringify(simulation)}`
          )
        );
      })
        .then(() => service.simulation.simulate(simulation))
        .catch(() => null),
  },
};

module.exports = resolvers;
