const { gql } = require('apollo-server');

const typeDefs = gql`
  "Detalhe da despesa"
  type ExpenseDetail {
    "Identificador da despesa"
    _id: ID

    "Número do cartão"
    cardNumber: String

    "Identificação do estabelecimento"
    cnpj: String

    "Data e hora da transação"
    timeStamp: Int

    "Imagem do mapa onde a transação ocorreu"
    mapLocation: String
  }

  "Despesas"
  type Expense {
    "Identificação do usuário"
    idUser: ID

    "Nome da empresa"
    companyName: String

    "Identificador da despesa"
    _id: ID

    "Detalhe da despesa"
    details: ExpenseDetail

    "Valor da despesa"
    value: Float
  }

  "Recursos da Conta"
  type AccountResources {
    overdraft: Int
    credit: Int
    investments: Int
  }

  "Dados obrigatórios para simulação"
  input SimulationInput {
    interestRate: Float
    loanAmount: Float
    days: Int
  }

  "Resultado da simulação"
  type Simulation {
    value: Float
    tax: Float
  }

  "Tipos de consulta"
  type Query {
    expenses(idUser: ID!): [Expense]
    accountResources: AccountResources
  }

  type Mutation {
    simulate(simulation: SimulationInput): Simulation
  }
`;

module.exports = typeDefs;
