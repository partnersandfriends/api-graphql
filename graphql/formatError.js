const { ApolloError } = require('apollo-server');

const formatError = err => {
  let messageCode;
  let pathError;
  if (err) {
    if (err.path) {
      pathError = err.path.reduce((Acumulador, valorAtual) => {
        if (valorAtual) {
          return `${Acumulador}/${valorAtual}`;
        }
        return Acumulador;
      });
    }
    if (err.message) {
      messageCode = err.message.replace(/[a-z-A-Z ' ']/g, '');
    }
  }
  const code = messageCode || 500;
  return new ApolloError('Serviço não disponível', code, {
    pathError,
  });
};
module.exports = formatError;
